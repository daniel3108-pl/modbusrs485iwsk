using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using ModbusRS485.Core.ModbusProtocol;
using Xunit;

namespace ModbusRS485.Test;

public class TestModbusFrame
{
    [Theory]
    [InlineData("0A", "0F", "0FADAE", "0A", false)]
    [InlineData("0A", "0F", "0F", "00", false)]
    [InlineData("00", "0F", "ADAE", "0A", false)]
    [InlineData("0A", "0F", "0FADAE", "000A", true)]
    [InlineData("0A", "0F", "0FADE", "0A", true)]
    public void ShouldCreateNewFrameOrThrow(string add, string fc, string data, string chS, bool throws)
    {
        if (throws)
        {
            Assert.Throws<ValidationException>(() =>
            { 
                new ModbusFrame(add, fc, data);
            });
        }
        else
        {
            new ModbusFrame(add, fc, data);
        }
    }

    [Theory]
    [InlineData(":0A01AF02A03D005E\\r\\n", "0A", "01", "AF02A03D00", false)]
    [InlineData(":0102003F\\r\\n", "01", "02", "00", false)]
    [InlineData(":01029F\\r\\n", "01", "02", "", false)]
    public void ShouldParseAFrame(string frame, string add, string fc, string data, bool throws)
    {
        var mbFrame = ModbusFrame.Parse(frame);
        Assert.Equal(add, mbFrame.Address);
        Assert.Equal(fc, mbFrame.FunctionCode);
        Assert.Equal(data, mbFrame.Data);
    }

    [Theory]
    [InlineData("dasd12312313123123131daasdasd")]
    [InlineData(null)]
    [InlineData("------------=========l;'1;23';12'312',c.zx,c.zxhasjdhf;jh3702065734658279vbxcznvb")]
    [InlineData(":0100")]
    [InlineData(":\\r\\n")]
    [InlineData(":01029F\\r")]
    [InlineData(":010332111229F\\n")]
    [InlineData(":01029FZ-`\\r\\n")]
    [InlineData("2137")]
    public void ShouldNotParseFrame(string frame)
    {
        Assert.Throws<ModbusFrameStringIncorrectFormat>(() => ModbusFrame.Parse(frame));
    }
    
    [Theory]
    [InlineData("01", "AD", "1A")]
    [InlineData("01", "FF", "13")]
    [InlineData("01", "DD", "17")]
    [InlineData("A1", "F3", "15")]
    [InlineData("FF", "00", "14")]
    [InlineData("GH", "-2", null)]
    [InlineData("01", "00", "3F")]
    [InlineData("01", "92FA01DC", "C5")]
    [InlineData("11", "2137", "D1")]
    [InlineData(null, null, null)]
    public void ShouldReturnCorrectLrc(string address, string data, string expectedLrc)
    {
        var actualLrc = ModbusFrame.GetLRC(address + data);
        Assert.Equal(expectedLrc, actualLrc);
    }
}