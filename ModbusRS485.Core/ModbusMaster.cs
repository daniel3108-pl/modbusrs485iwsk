using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.Core.NotificationProvider;

namespace ModbusRS485.Core;

public interface IModbusMaster
{
    string Address { get; }
    bool IsActive { get; } 
    int ReadTimeout { get; set; }
    int WriteTimeout { get; set; }
    int Retransmissions { get; set; }
    public int TransactionTimeout { get; set; }

    ModbusStationType GetStationType();
    void OnFrameRecieved(object sender, ModbusFrame recievedFrame);
    event ModbusStation.ModbusFrameRecievedEventHandler ModbusFrameRecieved;
    Task SendFrameAsync(ModbusFrame frame);
    void SendFrame(ModbusFrame frame);
    void OpenConnection();
    void CloseConnection();
    void Dispose();
    void SendTest();
    Task SendCommandBroadcast(ModbusFunction function, string dataValue);
    Task SendCommandAddressed(ModbusFunction function, string to, string dataValue);
    void AddSlave(string address, string serialPort);
    void RemoveSlave(string address);
}

public class ModbusMaster : ModbusStation, IModbusMaster
{

    [WireToProvider("master")]
    private static INotifier? _notifier;
    
    /// <summary>
    /// List of slaves data pairs [Address, serialPort]
    /// </summary>
    private List<KeyValuePair<string, string>> _slaves = new();

    public ModbusMaster(string address) : base(address)
    {
        base.ModbusFrameRecieved += OnFrameRecieved;
    }

    public override void SetSerial(ModbusStationSerialPortConfiguration config)
    {
        config.WriteTimeout = WriteTimeout;
        config.ReadTimeout = ReadTimeout;
        base.SetSerial(config);
    }

    public ModbusStationType GetStationType()
    {
        return ModbusStationType.Master;
    }

    public void SendTest()
    {
        _notifier?.SendMessage(this, NotificationType.Success,$"[Master: {Address}]", "Hello Test from ModbusMaster");
    }
    
    public void OnFrameRecieved(object sender, ModbusFrame recievedFrame)
    {
        if (recievedFrame.FunctionCode == "85" && recievedFrame.Data == "02")
            _currentTransact.Roll(TransactionErrorCode.WRONG_ADDRESS);
        else
            _currentTransact.Complete(recievedFrame);
    }

    public int ReadTimeout { get; set; }
    public int WriteTimeout { get; set; }
    public int Retransmissions { get; set; }
    public int TransactionTimeout { get; set; }
    
    
    public async Task SendCommandBroadcast(ModbusFunction function, string dataValue)
    {
        if (function == ModbusFunction.Command2)
            throw new Exception("Cannot use Command 2 with Broadcast transaction");

        Action<KeyValuePair<string, string>> broadcastCommand = slave =>
        {
            var frame = new ModbusFrame(slave.Key, "01", dataValue);
            SetSerial(new ModbusStationSerialPortConfiguration(slave.Value));
            OpenConnection();
            _currentTransact.DoBroadcast(() => SendFrame(frame));
            CloseConnection();
            Thread.Sleep(500);
        };
        
        _slaves
            .ForEach(broadcastCommand);
    }

    public async Task SendCommandAddressed(ModbusFunction function, string to, string dataValue)
    {
        var frameTemplate = function switch
        {
            ModbusFunction.Command1 => new ModbusFrame(to, "01", dataValue),
            ModbusFunction.Command2 => new ModbusFrame(to, "02", ""),
        };

        var slavePortConfiguration =  _slaves
                                        .Where(p => p.Key == to)
                                        .Select(p => new ModbusStationSerialPortConfiguration(p.Value))
                                        .First();
        SetSerial(slavePortConfiguration);
                        
        OpenConnection();
        _currentTransact
            .Do(() => SendFrame(frameTemplate))
            .IfDidSucceed(frame =>
            {
                if (function == ModbusFunction.Command2)
                    _notifier?.SendMessage(this, NotificationType.Success, $"[Master: {Address}]",
                        "Tekst odebrany, [Dane: " + frame.Data + "]");
            })
            .IfDidFail(err =>
                _notifier?.SendMessage(this, NotificationType.Error, $"[Master: {Address}]", err.ToString()));
        CloseConnection();
       
        
    }

    public void AddSlave(string address, string serialPort)
    {
        _slaves.Add(new KeyValuePair<string, string>(address, serialPort));
    }

    public void RemoveSlave(string address)
    {
        _slaves = _slaves
                .Where(s => s.Key != address)
                .ToList();
    }
    
    public void OpenConnection()
    {
        base.OpenConnection();
        _currentTransact = new ModbusTransaction(TransactionTimeout, Retransmissions);
    }

    public void CloseConnection()
    {
        _currentTransact = null;
        base.CloseConnection();
    }
}