using System.Diagnostics;
using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Transactions;

namespace ModbusRS485.Core.ModbusProtocol;


public enum ModbusTransactionType
{
     Broadcast,
     Addressed
}

public enum TransactionErrorCode
{
     NO_ERROR,
     TIMEOUT_EXCEED,
     SLAVE_ERROR,
     WRONG_ADDRESS
}

public class TransactionResult
{
     private readonly bool _didSucceded;
     private readonly ModbusFrame? _dataResult;
     private readonly TransactionErrorCode? _errorCode;
     
     private TransactionResult() { }
     private TransactionResult(ModbusFrame dataResult)
     {
          _didSucceded = true;
          _dataResult = dataResult;
          _errorCode = null;
     }
     private TransactionResult(TransactionErrorCode errorCode)
     {
          _didSucceded = false;
          _errorCode = errorCode;
     }

     public static TransactionResult Success(ModbusFrame dataResult) =>
          new TransactionResult(dataResult);
     
     public static TransactionResult Fail(TransactionErrorCode errorCode) =>
          new TransactionResult(errorCode);

     public TransactionResult IfDidSucceed(Action<ModbusFrame> action)
     {
          if (_didSucceded) action(_dataResult);
          return this;
     }
     
     public TransactionResult IfDidFail(Action<TransactionErrorCode> action)
     {
          if (_errorCode.HasValue && !_didSucceded) action(_errorCode.Value);
          return this;
     }
}

public class ModbusTransaction
{
     // In miliseconds
     public int Timeout { get; private set; }
     public int Retransmissions { get; private set; }
     private bool rolledBack = false;
     private bool _completed = false;
     private ModbusFrame? _recievedAnswer = null;
     private TransactionErrorCode _errorCode;

     public ModbusTransaction(int timeout, int retransmissions)
     {
          Timeout = timeout;
          Retransmissions = retransmissions;
     }

     public void DoBroadcast(Action action)
     {
          action();
     }
     
     public TransactionResult Do(Action action)
     {
          var retransm = 0;
          var stopwatch = new Stopwatch();
          
          do
          {
               action();
               stopwatch.Start();
               while (!_completed && stopwatch.ElapsedMilliseconds <= Timeout) {}
               stopwatch.Stop();
               
               if (_completed)
                    return TransactionResult.Success(_recievedAnswer!);
               retransm++;
          } while (!rolledBack && retransm < Retransmissions);
          
          if (rolledBack)
               return TransactionResult.Fail(_errorCode);
          
          return TransactionResult.Fail(TransactionErrorCode.TIMEOUT_EXCEED);
     }

     public void Roll(TransactionErrorCode errorCode)
     {
          _errorCode = errorCode;
          rolledBack = true;
     }

     public void Complete(ModbusFrame recievedFrame)
     {
          _recievedAnswer = recievedFrame;
          _completed = true;
     }
}
