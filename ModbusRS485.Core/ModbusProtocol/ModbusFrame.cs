using System;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using ModbusRS485.Core.Utils;

namespace ModbusRS485.Core.ModbusProtocol;

[Serializable]
public class ModbusFrame
{
    public char Start { get; private set; }
    [Required]
    [RegularExpression(@"[0-9A-Fa-f]{2}")]
    public string Address { get; set; }
    [Required]
    [RegularExpression(@"[0-9A-Fa-f]{2}")]
    public string FunctionCode { get; set; }
    [EvenLength]
    [RegularExpression(@"[0-9A-Fa-f]*")]
    public string Data { get; set; }
    [Required]
    [RegularExpression(@"[0-9A-Fa-f]{2}")]
    public string CheckSum { get; set; }
    
    public string End { get; private set; }

    public ModbusFrame(string address, string functionCode, string data)
    {
        Start = ':';
        Address = address ?? throw new ArgumentNullException(nameof(address));
        FunctionCode = functionCode ?? throw new ArgumentNullException(nameof(functionCode));
        Data = data ?? throw new ArgumentNullException(nameof(data));
        CheckSum = GetLRC(address + data) ?? throw new ArgumentNullException("Checksum incorrect");
        End = @"\r\n";

        if (!Validator.TryValidateObject(this, new ValidationContext(this), null, true))
        {
            throw new ValidationException("Arguments does not match requirements.");
        }
    }
    
    public string ToString() => $":{Address}{FunctionCode}{Data}{CheckSum}\\r\\n";
    
    public static ModbusFrame Parse(string frame)
    {
        if (frame == null) throw new ModbusFrameStringIncorrectFormat("Null string is incorrect");
            
        const string pattern = @":(?'Address'[0-9A-Fa-f]{2})(?'Function'[0-9A-Fa-f]{2})(?'Data'[0-9A-Fa-f]*)(?'Checksum'[0-9A-Fa-f]{2})\\r\\n";
        var reg = new Regex(pattern);
        var matchedData = reg.Matches(frame);
        
        Trace.WriteLine(matchedData.ToString());

        if (matchedData.Count == 0)
            throw new ModbusFrameStringIncorrectFormat($"Frame '{frame}' is in incorrect format!");

        if (matchedData[0].Groups.Count != 5)
            throw new ModbusFrameStringIncorrectFormat($"Frame '{frame}' is in incorrect format!");
        
        if (matchedData[0].Groups["data"].Length % 2 != 0)
            throw new ModbusFrameStringIncorrectFormat($"Frame '{frame}' is in incorrect format!");

        if (matchedData[0].Groups["Checksum"].Value !=
            GetLRC(matchedData[0].Groups["Address"].Value + matchedData[0].Groups["Data"].Value))
            throw new ModbusFrameStringIncorrectFormat("Checksums are not correct");
        
        return new ModbusFrame(
            matchedData[0].Groups["Address"].Value,
            matchedData[0].Groups["Function"].Value,
            matchedData[0].Groups["Data"].Value
        );
    }

    public static string? GetLRC(string input)
    {
        if (!Regex.Match(input, @"^[0-9A-F]+$").Success)
            return null;
        
        var inputBytes = Encoding.ASCII.GetBytes(input);
        byte lrcByte = 0;
        inputBytes.ForEach(b => lrcByte += b);
        lrcByte = (byte) (0xFF - lrcByte);
        lrcByte++;
        return lrcByte.ToString("X2");
    }
}

public class ModbusFrameStringIncorrectFormat : Exception
{
    public ModbusFrameStringIncorrectFormat(string? message) : base(message)
    {
    }
}