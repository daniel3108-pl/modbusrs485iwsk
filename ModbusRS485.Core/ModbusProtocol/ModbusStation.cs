using System.ComponentModel.DataAnnotations;
using System.IO.Ports;
using Microsoft.VisualBasic;
using ModbusRS485.Core.Utils;

namespace ModbusRS485.Core.ModbusProtocol;

public enum DataBits
{
    Seven = 7,
    Eight = 8
}


public struct ModbusStationSerialPortConfiguration
{
    public string PortName { get; set; }
    public Parity Parity { get; set; }
    public StopBits StopBits { get; set; }
    public Handshake Handshake { get; set; }
    public DataBits DataBits { get; set; }
    public int BaudRate { get; set; }
    public int ReadTimeout { get; set; }
    public int WriteTimeout { get; set; }

    public ModbusStationSerialPortConfiguration(string portName)
    {
        PortName = portName;
        BaudRate = 9600;
        Parity = Parity.None;
        DataBits = DataBits.Eight;
        StopBits = StopBits.One;
        Handshake = Handshake.RequestToSend;
        ReadTimeout = 100;
        WriteTimeout = 100;
    }
    
    public SerialPort PreparePortConnectionObject(String portName)
    {
        var serialPort = new SerialPort();
        serialPort.PortName = portName;
        serialPort.BaudRate = BaudRate;
        serialPort.Parity = Parity;
        serialPort.DataBits = (int) DataBits;
        serialPort.StopBits = StopBits;
        serialPort.Handshake = Handshake;
        serialPort.ReadTimeout = ReadTimeout;
        serialPort.WriteTimeout = WriteTimeout;
        serialPort.NewLine = @"\r\n";

        return serialPort;
    }

    public SerialPort PreparePortConnectionObject() => PreparePortConnectionObject(portName: PortName);
}

public interface IModbusStation
{
    public string Address { get; }
    public bool IsActive { get; }
    public void SetSerial(ModbusStationSerialPortConfiguration config);
    public event ModbusStation.ModbusFrameRecievedEventHandler ModbusFrameRecieved;
    public Task SendFrameAsync(ModbusFrame frame);
    public void SendFrame(ModbusFrame frame);
    public void OpenConnection();
    public void CloseConnection();
    public void Dispose();
}

public abstract class ModbusStation : IModbusStation
{
    [MaxLength(2)][MinLength(2)]
    public string Address { get; private set; }
    public bool IsActive { get; private set; }
    
    protected ModbusStationSerialPortConfiguration _portConfiguration;
    protected ISerialPort _port;
    protected ModbusTransaction _currentTransact;
    
    public ModbusStation(string address)
    {
        Address = address;
    }
    
    public ModbusStation(ModbusStationSerialPortConfiguration config, string address)
    {
        _portConfiguration = config;
        Address = address;
        
    }
    
    public virtual void SetSerial(ModbusStationSerialPortConfiguration config)
    {
        _portConfiguration = config;
        _port = _portConfiguration.PreparePortConnectionObject();
        // Sprawdzenie czy port jest dostepny
        _port.Open();
        _port.Close();
    }
    
    public void OpenConnection()
    {
        if (_port is null)
            throw new Exception("Cannot open connection for empty serial port");
        
        _port.DataReceived += OnDataRecieved;
        _port.Open();
        IsActive = true;
        
    }

    public void CloseConnection()
    {
        _port.Close();
        IsActive = false;
    }

    private void OnDataRecieved(object sender, SerialDataReceivedEventArgs e)
    {
        if (sender is not ISerialPort port)
            throw new ArgumentException("sender is not serial port");
        
        try
        {
            var data = port.ReadExisting();
            var frame = ModbusFrame.Parse(data);
            ModbusFrameRecieved.Invoke(this, frame);
        }
        catch (ModbusFrameStringIncorrectFormat ex)
        {
            SendFrame(new ModbusFrame("00", "85", "02"));
        }
    }

    public delegate void ModbusFrameRecievedEventHandler(object sender, ModbusFrame frame);
    public event ModbusFrameRecievedEventHandler ModbusFrameRecieved;

    public async Task SendFrameAsync(ModbusFrame frame)
    {
        _port.Write(frame.ToString());
    }

    public void SendFrame(ModbusFrame frame)
    {
        _port.Write(frame.ToString());
    }
    
    public void Dispose()
    {
        _port.Close();
    }
}