namespace ModbusRS485.Core.ModbusProtocol;

public enum ModbusMessageType
{
    Request,
    Response,
    ExceptionResponse
}