using System.ComponentModel.DataAnnotations;
using System.Reflection;
using ModbusRS485.Core.Utils;

namespace ModbusRS485.Core.ModbusProtocol;

public interface IModbusStationManager
{
    IModbusMaster CreateMasterStation([TimeoutConstraint(0, 10000, 100)] int timeout, int RetransmissionNumbers, int transactTimeout);

    IModbusSlave CreateSlaveStation(ModbusStationSerialPortConfiguration config, string address,
        [TimeoutConstraint(0, 1000, 10)] int timeout);
}

public class ModbusStationManager : IModbusStationManager
{
    private static ModbusStationManager _instance;
    private ushort _nextSlaveAddress = 1;
    public static ModbusStationManager Instance
    {
        get
        {
            if (_instance is null)
                _instance = new ModbusStationManager();
        
            return _instance;
        }
    }

    private ModbusStationManager() {}

    public IModbusMaster CreateMasterStation(int timeout, int RetransmissionNumbers, int transactTimeout)
    {
        if (timeout < 0 || timeout > 10000 || timeout % 10 != 0 || RetransmissionNumbers < 0 ||
            RetransmissionNumbers > 5)
            throw new ArgumentException("Provided data is not valid");

        var master = new ModbusMaster("00");
        master.ReadTimeout = timeout == 0 ? -1 : timeout;
        master.WriteTimeout = timeout == 0 ? -1 : timeout;
        master.Retransmissions = RetransmissionNumbers;
        master.TransactionTimeout = transactTimeout;
        return master;
    }

    public IModbusSlave CreateSlaveStation(ModbusStationSerialPortConfiguration config, string address,
                                             int timeout)
    {
        if (timeout < 0 || timeout > 1000 || timeout % 10 != 0 || address == "" || address.Length != 2)
            throw new ArgumentException("Provided data is not valid");
        
        config.ReadTimeout = timeout == 0 ? -1 : timeout;
        config.WriteTimeout = timeout == 0 ? -1 : timeout;
        return new ModbusSlave(config,(address).ToString());
    }
    
}