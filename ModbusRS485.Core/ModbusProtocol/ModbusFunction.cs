namespace ModbusRS485.Core.ModbusProtocol;

public enum ModbusFunction : ushort
{
    Command1 = 1,
    Command2 = 2
}