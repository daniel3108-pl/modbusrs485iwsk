namespace ModbusRS485.Core.ModbusProtocol;

public enum ModbusStationType
{
    Slave,
    Master
}