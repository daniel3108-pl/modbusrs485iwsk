using System.Reflection;
using ModbusRS485.Core.Utils;

namespace ModbusRS485.Core.NotificationProvider;

public class NotificationProvider
{
    public string Identifier { get; private set; }
    public delegate void NotificationEventHandler(object sender, NotificationType notificationType, string from, string notification);
    public event NotificationEventHandler NotificationRecieved;

    private readonly List<Notifier> _notifiers; 
    
    public NotificationProvider(string identifier)
    {
        Identifier = identifier;
        _notifiers = new List<Notifier>();
    }

    public void Initialize()
    {
        Assembly.GetExecutingAssembly()
            .GetTypes()
            .SelectMany(t => t.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            .Where(f => f.GetCustomAttributes<WireToProviderAttribute>().Any())
            .Where(f => f.GetCustomAttribute<WireToProviderAttribute>().ProviderId == Identifier)
            .ForEach(field =>  field.SetValue(null, RegisterNewNotifier()));
    }

    public INotifier RegisterNewNotifier() {
        var notifier = new Notifier();
        notifier.NotificationRecieved += OnNotificationRecieved;
        _notifiers.Add(notifier);
        return notifier;
    }
    
    private void OnNotificationRecieved(object sender, NotificationType notificationType, string from, string notfication)
    {
        NotificationRecieved.Invoke(sender, notificationType, from, notfication);
    }
    
    private class Notifier : INotifier
    {
        public delegate void NotificationEventHandler(object sender, NotificationType notificationType, string from, string notification);
        public event NotificationEventHandler NotificationRecieved;
        
        public void SendMessage(object sender, NotificationType notificationType, string from, string message)
        {
            NotificationRecieved.Invoke(sender, notificationType, from, message);
        }
    }
}