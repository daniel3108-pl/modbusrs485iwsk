namespace ModbusRS485.Core.NotificationProvider;

[AttributeUsage(AttributeTargets.Field)]
public class WireToProviderAttribute : Attribute
{
    public string ProviderId { get; private set; }
    
    public WireToProviderAttribute(string providerId)
    {
        ProviderId = providerId;
    }
}