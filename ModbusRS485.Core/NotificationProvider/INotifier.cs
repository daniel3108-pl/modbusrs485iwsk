namespace ModbusRS485.Core.NotificationProvider;

public enum NotificationType
{
    Error,
    Information,
    Success
}

public interface INotifier
{
    void SendMessage(object sender, NotificationType notificationType, string from, string message);
}