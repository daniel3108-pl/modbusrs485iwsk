using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO.Ports;
using System.Reflection;
using System.Threading.Tasks;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.Core.NotificationProvider;
using ModbusRS485.Core.Utils;
using SerialPort = ModbusRS485.Core.SerialPort;

namespace ModbusRS485.Core;

public interface IModbusSlave
{
    ModbusStationType GetStationType();
    void OnFrameRecieved(object sender, ModbusFrame recievedFrame);
    void Start();
    Task CommandReadFromMaster(ModbusFrame frame);
    Task CommandWriteToMaster(ModbusFrame requestFrame);
    void Dispose();
    string Address { get; }
    bool IsActive { get; }
    string ArgumentsToSend { get; set; }
    event ModbusStation.ModbusFrameRecievedEventHandler ModbusFrameRecieved;
    Task SendFrameAsync(ModbusFrame frame);
    void SendFrame(ModbusFrame frame);
    void OpenConnection();
    void CloseConnection();
}

public class ModbusSlave : ModbusStation, IModbusSlave
{
    [WireToProvider("slave")]
    private static INotifier? _notifier;

    public string ArgumentsToSend { get; set; } = "";

    public ModbusSlave(ModbusStationSerialPortConfiguration config, string address) : base(config, address)
    {
        base.ModbusFrameRecieved += OnFrameRecieved;
        _portConfiguration = config;
        _notifier?.SendMessage(this, NotificationType.Information, "[Slave 01]", "Hello world");
        base.SetSerial(config);
        
    }
    
    public ModbusStationType GetStationType()
    {
        return ModbusStationType.Slave;
    }

    public void OnFrameRecieved(object sender, ModbusFrame recievedFrame)
    {
        switch (recievedFrame.FunctionCode) {
            case "01":
                Task.Run(async () => await CommandReadFromMaster(recievedFrame));
                break;
            case "02":
                Task.Run(async () => await CommandWriteToMaster(recievedFrame));
                break;
        }
    }

    public void Start()
    {
        base.OpenConnection();
    }

    public async Task CommandReadFromMaster(ModbusFrame frame)
    {
        if (frame.Address != this.Address)
        {
            SendFrame(new ModbusFrame("00", "85", "02"));
            return;
        }
            

        ArgumentsToSend = frame.Data;
        await this.SendFrameAsync(frame);
        _notifier?.SendMessage(this, NotificationType.Success,$"[Slave: {Address}]",$"Tekst odebrany [Dane: {frame.Data}]");
    }

    public async Task CommandWriteToMaster(ModbusFrame requestFrame)
    {
        var frame = new ModbusFrame(requestFrame.Address, requestFrame.FunctionCode, ArgumentsToSend);
        await this.SendFrameAsync(frame);
    }

    public void Dispose()
    {
        base.CloseConnection();
    }

    
}