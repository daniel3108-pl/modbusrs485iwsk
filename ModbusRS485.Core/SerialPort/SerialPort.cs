using System.ComponentModel;
using System.IO.Ports;

namespace ModbusRS485.Core;

public class SerialPort : System.IO.Ports.SerialPort, ISerialPort
{
    public SerialPort()
    {
    }

    public SerialPort(IContainer container) : base(container)
    {
    }

    public SerialPort(string portName) : base(portName)
    {
    }

    public SerialPort(string portName, int baudRate) : base(portName, baudRate)
    {
    }

    public SerialPort(string portName, int baudRate, Parity parity) : base(portName, baudRate, parity)
    {
    }

    public SerialPort(string portName, int baudRate, Parity parity, int dataBits) : base(portName, baudRate, parity, dataBits)
    {
    }

    public SerialPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits) : base(portName, baudRate, parity, dataBits, stopBits)
    {
    }
}