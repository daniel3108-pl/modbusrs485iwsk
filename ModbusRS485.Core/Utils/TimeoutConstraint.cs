using System;
using System.ComponentModel.DataAnnotations;

namespace ModbusRS485.Core.Utils;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = false)]
public class TimeoutConstraint : ValidationAttribute
{
    public int LeftBound { get; set; }
    public int RightBound { get; set; }
    public int Step { get; set; }

    public TimeoutConstraint(int leftBound, int rightBound, int step)
    {
        LeftBound = leftBound;
        RightBound = rightBound;
        Step = step;
    }
    
    public override bool IsValid(object value)
    {
        var val = value as int?;

        if (val.HasValue)
        {
            bool meetsBounds = val.Value >= LeftBound && val.Value <= RightBound;
            bool meetsSteps = val.Value % Step == 0;

            return meetsBounds && meetsSteps;
        }
        
        return false;
    }
}