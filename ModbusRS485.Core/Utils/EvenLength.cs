using System;
using System.ComponentModel.DataAnnotations;

namespace ModbusRS485.Core.Utils;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
public class EvenLength : ValidationAttribute
{
    public EvenLength()
    {
    }

    public EvenLength(Func<string> errorMessageAccessor) : base(errorMessageAccessor)
    {
    }

    public EvenLength(string errorMessage) : base(errorMessage)
    {
    }

    public override bool IsValid(object value)
    {
        var val = value as string;
        return val.Length % 2 == 0 ? true : false;
    }
}