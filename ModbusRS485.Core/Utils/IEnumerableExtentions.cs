namespace ModbusRS485.Core.Utils;

public static class IEnumerableExtentions
{
    public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
    {
        enumerable.ToList()
            .ForEach(action);
    }
}