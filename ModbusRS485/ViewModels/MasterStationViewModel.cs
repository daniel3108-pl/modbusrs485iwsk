using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text.RegularExpressions;
using Avalonia.Controls;
using DynamicData;
using Microsoft.VisualBasic;
using ModbusRS485.Core;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.Core.NotificationProvider;
using ModbusRS485.Core.Utils;
using ModbusRS485.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace ModbusRS485.ViewModels; 

public class MasterStationViewModel : ViewModelBase 
{
    #region ViewData
    [Reactive] public IEnumerable<string> SerialPorts { get; set; }
    [Reactive] public IEnumerable<ModbusFunction> ModbusCommands { get; set; }
    [Reactive] public IEnumerable<ModbusTransactionType> ModbusTransaction { get; set; }
    
    [Reactive] public IEnumerable<KeyValuePair<string, string>> SlaveList { get; set; } = ImmutableList<KeyValuePair<string, string>>.Empty;
    [Reactive] public string PortName { get; set; }
    [Reactive] public string LogsField { get; set; }
    [Reactive] public ModbusFunction MasterFunction { get; set; }
    [Reactive] public ModbusTransactionType MasterTransaction { get; set; }
    [Reactive] public KeyValuePair<string, string> MasterCurrentSlave { get; set; }
    [Reactive] public string SlaveAddress { get; set; }
    [Reactive] public string DataArguments { get; set; } = "00";
    [Reactive] public string RecievedData { get; set; }
    [Reactive] public int Timeout { get; set; }
    [Reactive] public int RetransmissionNumber { get; set; }
    [Reactive] public int CharacterDifferenceTimeout { get; set; }
    [Reactive] public bool IsModbusConnected { get; set; }

    #endregion

    private MasterStationDataSender _masterStationDataSender;
    private IModbusMaster _master;
    private NotificationProvider _provider;

    private string _dataArguments { get; set; } = "00";

    public MasterStationViewModel()
    {
        SerialPorts = SerialPort.GetPortNames()
                                .ToImmutableList();
        
        ModbusCommands = Enum.GetValues(typeof(ModbusFunction))
                                .Cast<ModbusFunction>()
                                .ToImmutableList();
        
        ModbusTransaction = Enum.GetValues(typeof(ModbusTransactionType))
                                .Cast<ModbusTransactionType>()
                                .ToImmutableList();
                            
        _provider = new NotificationProvider("master");
        _provider.NotificationRecieved += NotifierOnNotificationRecieved;
        _provider.Initialize();

        CreateObservables();
        CreateReactiveCommands();
    }

    #region ReactiveCommands
    
    public ReactiveCommand<Unit, Unit> StartModbusStation { get; set; }
    public ReactiveCommand<Unit, Unit> MakeCommand { get; set; }
    public ReactiveCommand<Unit, Unit> ClearLogsCommand { get; set; }
    public ReactiveCommand<Unit, Unit> ArgsTextBoxSaveCommand { get; set; }
    public ReactiveCommand<Unit, Unit> AddNewSlaveCommand { get; set; }
    public ReactiveCommand<Unit, Unit> DeleteSlaveCommand { get; set; }

    private void CreateReactiveCommands()
    {
        StartModbusStation = ReactiveCommand.Create(
            execute: () =>
            {
                try
                {
                    _master = ModbusStationManager.Instance.CreateMasterStation(CharacterDifferenceTimeout, RetransmissionNumber, Timeout);
                    _masterStationDataSender = new MasterStationDataSender(_provider.RegisterNewNotifier(), _master);
                    IsModbusConnected = true;
                }
                catch (Exception ex)
                {
                    NotifierOnNotificationRecieved(null, NotificationType.Error, "App",
                        $"Could not connect to slave: [\"{ex.Message}\"]");
                }
            }
        );

        MakeCommand = ReactiveCommand.Create(
            execute: () =>
            {
                switch (MasterTransaction)
                {
                    case ModbusTransactionType.Addressed:
                        _masterStationDataSender.SendCommandAddressed(MasterFunction, MasterCurrentSlave.Key,
                            _dataArguments);
                        break;
                    case ModbusTransactionType.Broadcast:
                        _masterStationDataSender.SendCommandBroadcast(MasterFunction, _dataArguments.ToUpper());
                        break;
                }

            },
            canExecute: this.WhenAnyValue(
                x => x.IsModbusConnected,
                x => x._dataArguments,
                x => x.MasterTransaction,
                x => x.MasterFunction,
                (isMoudbusConnected, dataArgs, masterTransaction, masterFunc) =>
                    isMoudbusConnected && (masterTransaction != null && masterFunc != null) &&
                                MasterCurrentSlave.Key != null &&
                                !(masterTransaction == ModbusTransactionType.Broadcast && masterFunc == ModbusFunction.Command2) &&
                                dataArgs?.Length % 2 == 0 && 
                                dataArgs?.Length <= 2 * 255)
        );

        ClearLogsCommand = ReactiveCommand.Create(
            execute: () =>
            {
                LogsField = "";
            }
        );
        
        ArgsTextBoxSaveCommand = ReactiveCommand.Create(
            execute: () =>
            {
                _dataArguments = DataArguments;
            },
            canExecute: this.WhenAnyValue(x => x.DataArguments,
                (data) => (data?.Length % 2 == 0) && data?.Length <= (2 * 255) && 
                          Regex.Match(data, @"[0-9A-Fa-f]+").Success)
        );

        AddNewSlaveCommand = ReactiveCommand.Create(
            execute: () =>
            {
                SlaveList = (SlaveList as ImmutableList<KeyValuePair<string, string>>)!.Add(
                    new KeyValuePair<string, string>(SlaveAddress, PortName));
                _master.AddSlave(SlaveAddress, PortName);
            },
            canExecute: this.WhenAnyValue(
                x => x.IsModbusConnected, x => x.SlaveAddress, x => x.PortName,
                (isMoudbusConnected, slaveAddress, port) => isMoudbusConnected && slaveAddress?.Length == 2 && port != "" && slaveAddress != "00")
        );

        DeleteSlaveCommand = ReactiveCommand.Create(
            execute: () =>
            {
                SlaveList = SlaveList
                                .Where(p => p.Key != SlaveAddress)
                                .ToImmutableList();
                _master.RemoveSlave(SlaveAddress);
            },
            canExecute: this.WhenAnyValue(
                x => x.IsModbusConnected, x => x.SlaveAddress, x => x.PortName,
                (isMoudbusConnected, slaveAddress, port) => isMoudbusConnected && 
                                                            slaveAddress?.Length == 2 && port != "")
        );
    }

    #endregion

    #region Observables

    public IObservable<bool> IsModbusStationConnected;

    private void CreateObservables()
    {
        IsModbusStationConnected =
            this.WhenAnyValue(x => x.IsModbusConnected, (isModbusConnected) => isModbusConnected == true);
    }

    #endregion

    private void NotifierOnNotificationRecieved(object sender, NotificationType notificationtype, string @from,
        string notification)
    {
        const string regPattern = @".+\[Dane: (?'data'[A-Z0-9]*)\]";
        var match = Regex.Match(notification, regPattern);
        if (match.Groups.Count > 0)
            RecievedData = match.Groups[1].Value;

        var notifType = Enum.GetName(typeof(NotificationType), notificationtype);
        LogsField += $"[{notificationtype} From: {from}]: \"{notification}\"\n";
    }
}