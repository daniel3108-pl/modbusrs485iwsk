using System;
using System.Reactive;
using System.Windows.Input;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace ModbusRS485.ViewModels;

public class ChooseStationViewModel : ViewModelBase, IRoutableViewModel 
{
    
    public string UrlPathSegment => "ChooseStation";
    public IScreen HostScreen { get; }

    [Reactive] public bool SlaveIsChecked { get; set; }
    [Reactive] public string SlaveAddress { get; set; }
    
    public IObservable<bool> UseSlave;
    public ReactiveCommand<Unit, Unit> StartModbusStation { get; }

    public ChooseStationViewModel() 
    {
        HostScreen = Locator.Current.GetService<IScreen>();
        
        // UseSlave =
        //     this.WhenAnyValue(
        //         x => x.SlaveIsChecked,
        //         slaveIsChecked => slaveIsChecked == true);
        //
        // var canStartSession =
        //     this.WhenAnyValue(
        //         x => x.SlaveAddress, x => x.SlaveIsChecked,
        //         (slaveAddress, slaveIsChecked) => !slaveIsChecked || slaveAddress?.Length == 2);
        //
        // StartModbusStation = ReactiveCommand.Create(
        //     execute: () => {
        //         HostScreen.Router.Navigate.Execute(new ModbusSlaveViewModel() { AddressSlave = SlaveAddress});
        //     },
        //     canExecute: canStartSession
        // );
    }
}