using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.Core.NotificationProvider;
using ModbusRS485.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace ModbusRS485.ViewModels;

public class SlaveStationViewModel : ViewModelBase
{
    #region ViewDataProperties

    [Reactive] public IEnumerable<String> PortNames { get; set; }
    [Reactive] public string LogsField { get; set; }
    [Reactive] public string AddressSlave { get; set; }
    [Reactive] public string PortName { get; set; }
    [Reactive] public int CharacterBetweenTime { get; set; }
    [Reactive] public string SlaveDataArguments { get; set; }
    [Reactive] public string SlaveCheckSum { get; set; }
    [Reactive] public string RecievedDataFromMaster { get; set; }
    [Reactive] public bool IsModbusConnected { get; set; }
    [Reactive] public bool IsReading { get; set; }

    #endregion ViewDataProperties

    private SlaveStationDataReader? _stationConnection;
    private readonly NotificationProvider _notifier;

    public SlaveStationViewModel()
    {
        _notifier = new NotificationProvider("slave");
        _notifier.NotificationRecieved += NotifierOnNotificationRecieved;
        _notifier.Initialize();

        PortNames = SerialPort.GetPortNames().ToList();

        CreateObservables();
        CreateReactiveCommands();
    }

    #region ReactiveCommands

    public ReactiveCommand<Unit, Unit> StartModbusStation { get; set; }
    public ReactiveCommand<Unit, Unit> StartReading { get; set; }
    public ReactiveCommand<Unit, Unit> ClearLogsCommand { get; set; }
    public ReactiveCommand<Unit, Unit> ArgsTextBoxSaveCommand { get; set; }
    
    private void CreateReactiveCommands()
    {
        StartModbusStation = ReactiveCommand.Create(
            execute: () =>
            {
                try
                {
                    _stationConnection = new SlaveStationDataReader(
                        ModbusStationManager.Instance.CreateSlaveStation(new ModbusStationSerialPortConfiguration(PortName),
                            AddressSlave.ToUpper(), CharacterBetweenTime), _notifier.RegisterNewNotifier());
                    IsModbusConnected = true;
                    NotifierOnNotificationRecieved(this, NotificationType.Success, $"Slave #{AddressSlave}",
                        "Connected successfully");
                }
                catch (Exception e)
                {
                    NotifierOnNotificationRecieved(this, NotificationType.Error, $"Slave #{AddressSlave}",
                        $"Connection refused: [\"{e.Message}\"]");
                }
            }, canExecute: this.WhenAnyValue(x => x.AddressSlave,
                (slaveAddress) => slaveAddress?.Length == 2 && slaveAddress != "00")
        );

        StartReading = ReactiveCommand.Create(
            execute: () =>
            {
                if (!IsReading)
                {
                    IsReading = true;
                    _stationConnection.StartReading();
                }
                else
                {
                    IsReading = false;
                    _stationConnection.StopReading();
                }
            },
            canExecute: this.WhenAnyValue(x => x.IsModbusConnected, x => x.SlaveCheckSum,
                (isMoudbusConnected, checkSum) => isMoudbusConnected == true && checkSum.Length == 2)
        );

        ClearLogsCommand = ReactiveCommand.Create(
            execute: () =>
            {
                LogsField = "";
            }
        );

        ArgsTextBoxSaveCommand = ReactiveCommand.Create(
            execute: () =>
            {
                _stationConnection.Slave.ArgumentsToSend = SlaveDataArguments;
            },
            canExecute: this.WhenAnyValue(x => x.SlaveDataArguments,
                (slaveData) => (slaveData?.Length % 2 == 0) && slaveData?.Length <= (2 * 255))
        );
    }

    #endregion

    #region Observables

    public IObservable<bool> IsModbusConnectedObservable;
    public IObservable<bool> IsModbusReading;

    private void CreateObservables()
    {
        IsModbusConnectedObservable =
            this.WhenAnyValue(x => x.IsModbusConnected, (isModbusConnected) => isModbusConnected == true);

        IsModbusReading = this.WhenAnyValue(x => x.IsReading).ObserveOn(RxApp.MainThreadScheduler);
    }

    #endregion

    private void NotifierOnNotificationRecieved(object sender, NotificationType notificationtype, string @from,
        string notification)
    {
        const string regPattern = @".+\[Dane: ([A-Z0-9]*)\]";
        var reg = new Regex(regPattern);
        var match = reg.Match(notification);
        if (match.Groups.Count > 0)
            RecievedDataFromMaster = match.Groups[0].Value;
        
        var notifType = Enum.GetName(typeof(NotificationType), notificationtype);
        LogsField += $"[{notificationtype} From: {from}]: \"{notification}\"\n";
    }
}