using Avalonia.Controls;
using Avalonia.ReactiveUI;
using ModbusRS485.ViewModels;
using ModbusRS485.Views;
using ReactiveUI;
using Splat;

namespace ModbusRS485; 

public class AppRouter : ReactiveObject, IScreen
{
    public RoutingState Router { get; }
    
    public AppRouter() 
    {
        Router = new RoutingState();
        Locator.CurrentMutable.RegisterConstant(this, typeof(IScreen));
        RegisterServices();
        RegisterViews();

        Router.Navigate.Execute(new ChooseStationViewModel());
    }

    private void RegisterServices() 
    {
        //Splat.Locator.CurrentMutable.Register(() => new StaticContacts);
    }

    private void RegisterViews() 
    {
        Locator.CurrentMutable.Register(() => new ChooseStationView(), typeof(IViewFor<ChooseStationViewModel>));
        Locator.CurrentMutable.Register(() => new SlaveStationView(), typeof(IViewFor<SlaveStationViewModel>));
    }
}