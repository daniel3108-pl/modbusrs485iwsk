using System.Reflection;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using ModbusRS485.ViewModels;
using ModbusRS485.Views;
using ReactiveUI;
using Splat;

namespace ModbusRS485
{
    public partial class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);

            
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                var appRouter = new AppRouter();

                desktop.MainWindow = new MainWindow
                {
                    DataContext = new MainWindowViewModel() { Router = appRouter.Router},
                };
            }
            base.OnFrameworkInitializationCompleted();
        }
    }
}