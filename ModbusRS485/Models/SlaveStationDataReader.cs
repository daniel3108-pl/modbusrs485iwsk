using System;
using ModbusRS485.Core;
using ModbusRS485.Core.NotificationProvider;

namespace ModbusRS485.Models;

public class SlaveStationDataReader
{
    private INotifier? _notifier;
    public IModbusSlave Slave { get; }
    public bool IsReading { get; set; } = false;

    public SlaveStationDataReader(IModbusSlave slave, INotifier notifier)
    {
        _notifier = notifier;
        Slave = slave;
    }

    public void StartReading()
    {
        try
        {
            Slave.Start();
        }
        catch (Exception e)
        {
            _notifier.SendMessage(this, NotificationType.Error, "SlaveConnection", e.Message);
        }

        IsReading = true;
    }

    public void StopReading()
    {
        IsReading = false;
        Slave.CloseConnection();
    }
}