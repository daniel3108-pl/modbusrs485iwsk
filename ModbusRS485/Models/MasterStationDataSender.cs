using System.Threading.Tasks;
using ModbusRS485.Core;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.Core.NotificationProvider;

namespace ModbusRS485.Models;

public class MasterStationDataSender
{
    private IModbusMaster _master;
    private INotifier _notifier;
    
    public MasterStationDataSender(INotifier notifier, IModbusMaster master)
    {
        _notifier = notifier;
        _master = master;
    }

    public void SendCommandAddressed(ModbusFunction commandType, string to, string data)
    {
        Task.Run(async () => await _master.SendCommandAddressed(commandType, to, data));
    }

    public void SendCommandBroadcast(ModbusFunction commandType, string? data = null)
    {
        Task.Run(async () => await _master.SendCommandBroadcast(commandType, data));
    }
}