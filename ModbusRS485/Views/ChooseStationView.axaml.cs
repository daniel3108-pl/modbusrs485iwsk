using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Mixins;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ModbusRS485.ViewModels;
using ReactiveUI;

namespace ModbusRS485.Views;

public partial class ChooseStationView : ReactiveUserControl<ChooseStationViewModel>
{
    public ChooseStationView()
    {
        InitializeComponent();
    }
}