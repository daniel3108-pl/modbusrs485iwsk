using System;
using System.Reactive;
using System.Reactive.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Mixins;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ModbusRS485.ViewModels;
using ReactiveUI;

namespace ModbusRS485.Views;

public partial class SlaveStationView : ReactiveUserControl<SlaveStationViewModel>
{
    public SlaveStationView()
    {
        InitializeComponent();
        
        this.WhenActivated(disposables =>
        {
            this.WhenAnyObservable(x => x.ViewModel.IsModbusConnectedObservable)
                .BindTo(this, x => x.SlaveAnswerArguments.IsEnabled)
                .DisposeWith(disposables);

            this.WhenAnyObservable(x => x.ViewModel.IsModbusReading)
                .Subscribe(x => { ReadingButton.Content = x ? "Zakończ nasłuch" : "Rozpocznij Nasłuch"; });
        });
    }
}