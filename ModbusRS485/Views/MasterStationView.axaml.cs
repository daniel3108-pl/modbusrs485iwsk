using System;
using System.IO.Ports;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Mixins;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ModbusRS485.Core.ModbusProtocol;
using ModbusRS485.ViewModels;
using ReactiveUI;

namespace ModbusRS485.Views;

public partial class MasterStationView : ReactiveUserControl<MasterStationViewModel>
{
    public MasterStationView()
    {
        InitializeComponent();
        
        this.WhenActivated(disposables =>
        {
            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.MasterAddress.IsEnabled)
                .DisposeWith(disposables);
            
            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.MasterCommand.IsEnabled)
                .DisposeWith(disposables);
            
            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.MasterTransmissionType.IsEnabled)
                .DisposeWith(disposables);
            
            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.SlaveAnswerArguments.IsEnabled)
                .DisposeWith(disposables);

            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.MasterPorts.IsEnabled)
                .DisposeWith(disposables);
            
            this.WhenAnyObservable(x => x.ViewModel.IsModbusStationConnected)
                .BindTo(this, x => x.MasterSlave.IsEnabled)
                .DisposeWith(disposables);
        });
    }
}