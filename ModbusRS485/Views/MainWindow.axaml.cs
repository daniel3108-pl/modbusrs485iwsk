using System;
using Avalonia.Controls;
using Avalonia.Controls.Mixins;
using Avalonia.ReactiveUI;
using ModbusRS485.ViewModels;
using ReactiveUI;

namespace ModbusRS485.Views
{
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        private RoutedViewHost RoutedViewHost => this.FindControl<RoutedViewHost>("RoutedView");

        public MainWindow()
        {
            InitializeComponent();
            
            this.WhenActivated(disposables =>
            {
                this.OneWayBind(ViewModel, x => x.Router, x => x.RoutedViewHost.Router).DisposeWith(disposables);
            });
        }
    }
}